package katas.kata06_you_shall_not_pass;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class YouShallNotPassTest {

    @Test
    void passwordCheck_password_shouldReturnWeak() {
        // Arrange
        String password = "password";
        String expected = "Weak";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    void passwordCheck_11081992_shouldReturnWeak() {
        // Arrange
        String password = "11081992";
        String expected = "Weak";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    void passwordCheck_S3cur1ty_shouldReturnStrong() {
        // Arrange
        String password = "@S3cur1ty";
        String expected = "Strong";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    void passwordCheck_moderatePassword_shouldReturnModerate() {
        // Arrange
        String password = "!@!pass1";
        String expected = "Moderate";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    void passwordCheck_mySecurePass12_shouldReturnModerate() {
        // Arrange
        String password = "mySecurePass12";
        String expected = "Moderate";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    void passwordCheck_pass_word_shouldReturnInvalid() {
        // Arrange
        String password = "pass word";
        String expected = "Invalid";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    void passwordCheck_stonk_shouldReturnInvalid() {
        // Arrange
        String password = "stonk";
        String expected = "Invalid";
        // Act
        YouShallNotPass youShallNotPass = new YouShallNotPass();
        String actual = youShallNotPass.passwordCheck(password);
        // Actual
        assertEquals(expected, actual);
    }
}