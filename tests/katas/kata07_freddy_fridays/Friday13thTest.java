package katas.kata07_freddy_fridays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Friday13thTest {

    @Test
    void countFriday13th_2020_ShouldReturn2() {
        Friday13th friday13th = new Friday13th();

        int year = 2020;
        int actual = friday13th.countFriday13th(year);
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    void countFriday13th_2022_ShouldReturn1() {
        Friday13th friday13th = new Friday13th();

        int year = 2022;
        int actual = friday13th.countFriday13th(year);
        int expected = 1;

        assertEquals(expected, actual);
    }
}