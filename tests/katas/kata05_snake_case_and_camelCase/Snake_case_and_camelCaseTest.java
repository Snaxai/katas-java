package katas.kata05_snake_case_and_camelCase;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Snake_case_and_camelCaseTest {

    @Test
    public void toCamelCase_hello_edabit_convertedToCamelCase() {
        // Arrange
        String input = "hello_edabit";
        String expected = "helloEdabit";
        // Act
        Snake_case_and_camelCase snake_case_and_camelCase = new Snake_case_and_camelCase();
        String actual = snake_case_and_camelCase.toCamelCase(input);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    public void toSnakeCase_helloEdabit_convertedToSnakeCase() {
        // Arrange
        String input = "helloEdabit";
        String expected = "hello_edabit";
        // Act
        Snake_case_and_camelCase snake_case_and_camelCase = new Snake_case_and_camelCase();
        String actual = snake_case_and_camelCase.toSnakeCase(input);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    public void toCamelCase_is_modal_open_convertedToCamelCase() {
        // Arrange
        String input = "is_modal_open";
        String expected = "isModalOpen";
        // Act
        Snake_case_and_camelCase snake_case_and_camelCase = new Snake_case_and_camelCase();
        String actual = snake_case_and_camelCase.toCamelCase(input);
        // Actual
        assertEquals(expected, actual);
    }

    @Test
    public void toSnakeCase_getColor_convertedToSnakeCase() {
        // Arrange
        String input = "getColor";
        String expected = "get_color";
        // Act
        Snake_case_and_camelCase snake_case_and_camelCase = new Snake_case_and_camelCase();
        String actual = snake_case_and_camelCase.toSnakeCase(input);
        // Actual
        assertEquals(expected, actual);
    }

}