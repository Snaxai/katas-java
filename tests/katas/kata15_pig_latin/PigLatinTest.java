package katas.kata15_pig_latin;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PigLatinTest {

    @Test
    void translateWordToPigLatin_flag_shouldReturnagflay() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "flag";
        String expected = "agflay";
        // Act
        String actual = pigLatin.translateWordToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void translateWordToPigLatin_Flag_shouldReturnagflayWithQuestionMark() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "Flag?";
        String expected = "Agflay?";
        // Act
        String actual = pigLatin.translateWordToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void translateWordToPigLatin_Apple_shouldReturnAppleyay() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "Apple";
        String expected = "Appleyay";
        // Act
        String actual = pigLatin.translateWordToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void translateWordToPigLatin_uttonbay_shouldReturnUttonbay() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "button";
        String expected = "uttonbay";
        // Act
        String actual = pigLatin.translateWordToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void translateWordToPigLatin_emptyString_shouldReturnEmptyString() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "";
        String expected = "";
        // Act
        String actual = pigLatin.translateWordToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void translateSentenceToPigLatin_ILikeToEatHoneyWaffles_shouldReturnIyayIkelayOtayEatyayOneyhayAfflesway() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "I like to eat honey waffles";
        String expected = "Iyay ikelay otay eatyay oneyhay afflesway";
        // Act
        String actual = pigLatin.translateSentenceToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void translateSentenceToPigLatin_flag_shouldReturnagflay() {
        // Arrange
        PigLatin pigLatin = new PigLatin();
        String word = "Do you think it is going to rain today?";
        String expected = "Oday ouyay inkthay ityay isyay oinggay otay ainray odaytay?";
        // Act
        String actual = pigLatin.translateSentenceToPigLatin(word);
        // Assert
        assertEquals(expected, actual);
    }
}