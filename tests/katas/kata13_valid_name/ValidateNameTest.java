package katas.kata13_valid_name;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateNameTest {

    @Test
    void validName_HdotWells_shouldReturnTrue() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "H. Wells";
        boolean expected = true;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HdotGdotWells_shouldReturnTrue() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "H. G. Wells";
        boolean expected = true;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HerbertGdotWells_shouldReturnTrue() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "Herbert G. Wells";
        boolean expected = true;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HerbertGeorgeWells_shouldReturnTrue() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "Herbert George Wells";
        boolean expected = true;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_Herbert_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "Herbert";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HerbertWdotGdotWells_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "Herbert W. G. Wells";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_hdotWells_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "h. Wells";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_herbertGdotWells_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "herbert G. Wells";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HWells_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "H Wells";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HerbdotWells_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "Herb. Wells";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HdotGeorgeWells_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "H. George Wells";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validName_HerbertGeorgeWdot_shouldReturnFalse() {
        // Arrange
        ValidateName validateName = new ValidateName();
        String name = "Herbert George W.";
        boolean expected = false;
        // Act
        boolean actual = validateName.validName(name);
        // Assert
        assertEquals(expected, actual);
    }

}