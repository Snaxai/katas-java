package katas.kata04_reducto_multiplicitum;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReductoMultiplicitumTest {

    @Test
    public void sumDigProd_validValues_ShouldEqualsZero() {
        // Arrange
        int[] numbers = new int[]{0};
        int expected = 0;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(actual, expected);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsNine() {
        // Arrange
        int[] numbers = new int[]{9};
        int expected = 9;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(actual, expected);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsSeven() {
        // Arrange
        int[] numbers = new int[]{9, 8};
        int expected = 7;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsSix() {
        // Arrange
        int[] numbers = new int[]{16, 28};
        int expected = 6;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsOne() {
        // Arrange
        int[] numbers = new int[]{111111111};
        int expected = 1;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsTwo() {
        // Arrange
        int[] numbers = new int[]{1, 2, 3, 4, 5, 6};
        int expected = 2;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsSix1() {
        // Arrange
        int[] numbers = new int[]{8, 16, 89, 3};
        int expected = 6;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsSix2() {
        // Arrange
        int[] numbers = new int[]{26, 497, 62, 841};
        int expected = 6;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsSix3() {
        // Arrange
        int[] numbers = new int[]{17737, 98723, 2};
        int expected = 6;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsEight1() {
        // Arrange
        int[] numbers = new int[]{123, -99};
        int expected = 8;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsEight2() {
        // Arrange
        int[] numbers = new int[]{167, 167, 167, 167, 167, 3};
        int expected = 8;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void sumDigProd_validValues_ShouldEqualsTwo2() {
        // Arrange
        int[] numbers = new int[]{98526, 54, 863, 156489, 45, 6156};
        int expected = 2;
        // Act
        ReductoMultiplicitum reductoMultiplicitum = new ReductoMultiplicitum();
        int actual = reductoMultiplicitum.sumDigProd(numbers);
        // Assert
        assertEquals(expected, actual);
    }

}