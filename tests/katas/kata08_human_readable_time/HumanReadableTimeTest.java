package katas.kata08_human_readable_time;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanReadableTimeTest {

    @Test
    void convertToReadableTime_0seconds_ShoulReturn000000() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "00:00:00";

        String actual = humanReadableTime.convertToReadableTime(0);

        assertEquals(expected, actual);
    }

    @Test
    void convertToReadableTime_5seconds_ShoulReturn000005() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "00:00:05";

        String actual = humanReadableTime.convertToReadableTime(5);

        assertEquals(expected, actual);
    }

    @Test
    void convertToReadableTime_60seconds_ShoulReturn000100() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "00:01:00";

        String actual = humanReadableTime.convertToReadableTime(60);

        assertEquals(expected, actual);
    }

    @Test
    void convertToReadableTime_86399seconds_ShoulReturn235959() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "23:59:59";

        String actual = humanReadableTime.convertToReadableTime(86399);

        assertEquals(expected, actual);
    }

    @Test
    void convertToReadableTime_359999seconds_ShoulReturn995959() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "99:59:59";

        String actual = humanReadableTime.convertToReadableTime(359999);

        assertEquals(expected, actual);
    }

    @Test
    void convertToReadableTime_85412seconds_ShoulReturn() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "23:43:32";

        String actual = humanReadableTime.convertToReadableTime(85412);

        assertEquals(expected, actual);
    }

    @Test
    void convertToReadableTime_459999seconds_ShoulReturnError() {
        HumanReadableTime humanReadableTime = new HumanReadableTime();
        String expected = "Error";

        String actual = humanReadableTime.convertToReadableTime(459999);

        assertEquals(expected, actual);
    }
}