package katas.kata10_bookstore;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookstoreTest {

    // Basic tests
    @Test
    void price_emptyArray_shouldReturn0() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        int expected = 0;
        int[] books = {};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfBook1_shouldReturn8() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        int expected = 8;
        int[] books = {1};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfBook2_shouldReturn8() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        int expected = 8;
        int[] books = {2};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfBook3_shouldReturn8() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        int expected = 8;
        int[] books = {3};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfThreeOfTheSameBooks_shouldReturn0() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        int expected = 8 * 3;
        int[] books = {1, 1, 1};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    // Discount tests
    @Test
    void price_arrayOfBook0and1_shouldReturnDiscount() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        double expected = 8 * 2 * 0.95;
        int[] books = {0, 1};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfBook0and2and3_shouldReturnDiscount() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        double expected = 8 * 3 * 0.9;
        int[] books = {0, 2, 3};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfFourDifferentBooks_shouldReturnDiscount() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        double expected = 8 * 4 * 0.8;
        int[] books = {0, 1, 2, 3};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    // Optional extra
    @Test
    void price_arrayOfThreeBooksWhereTwoOfThemAreTheSame_shouldReturnDiscountForTwoOfThem() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        double expected = 8 + (8 * 2 * 0.95);
        int[] books = {0, 0, 1};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfFourBooksWhereTwoAndTwoAreSameType_shouldReturn5PercentDiscountTime2() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        double expected = 2 * (8 * 2 * 0.95);
        int[] books = {0, 0, 1, 1};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void price_arrayOfSixBooksWhereThereAreFourDifferentAndTwoAndTwoAreSameType_shouldReturnDiscountForFourDifferentAndTwoDifferent() {
        // Arrange
        Bookstore bookstore = new Bookstore();
        double expected = ((8 * 4 * 0.8) + (8 * 2 * 0.95));
        int[] books = {0, 0, 1, 2, 2, 3};
        // Act
        double actual = bookstore.price(books);
        // Assert
        assertEquals(expected, actual);
    }

}