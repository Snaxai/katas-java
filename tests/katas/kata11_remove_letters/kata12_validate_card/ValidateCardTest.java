package katas.kata11_remove_letters.kata12_validate_card;

import katas.kata12_validate_card.ValidateCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateCardTest {

    @Test
    void validateCard_shortNumber_shouldReturnFalse() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 79927398714L;
        boolean expected = false;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_invalidNumber_shouldReturnFalse2() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 79927398713L;
        boolean expected = false;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_validNumber_shouldReturnTrue() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 709092739800713L;
        boolean expected = true;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_invalidNumber_shouldReturnFalse3() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 1234567890123456L;
        boolean expected = false;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_validNumber_shouldReturnTrue2() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 12345678901237L;
        boolean expected = true;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_validNumber_shouldReturnTrue3() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 5496683867445267L;
        boolean expected = true;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_invalidNumber_shouldReturnFalse4() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 4508793361140566L;
        boolean expected = false;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_validNumber_shouldReturnTrue4() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 376785877526048L;
        boolean expected = true;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void validateCard_invalidNumber_shouldReturnFalse5() {
        // Arrange
        ValidateCard validateCard = new ValidateCard();
        long number = 36717601781975L;
        boolean expected = false;
        // Act
        boolean actual = validateCard.validateCard(number);
        // Assert
        assertEquals(expected, actual);
    }

}