package katas.kata11_remove_letters;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RemoveLettersTest {

    @Test
    void removeLetters_listOfStringsAndWord_shouldReturnW() {
        RemoveLetters removeLetters = new RemoveLetters();
        List<String> expected = new ArrayList<>(Collections.singleton("w"));
        List<String> array = new ArrayList<>(List.of(new String[]{"s", "t", "r", "i", "n", "g", "w"}));
        String word = "string";

        List<String> actual = removeLetters.removeLetters(array, word);

        assertEquals(expected, actual);
    }

    @Test
    void removeLetters_listOfStringsAndWord_shouldReturnbgw() {
        RemoveLetters removeLetters = new RemoveLetters();
        List<String> expected = new ArrayList<>(List.of(new String[]{"b", "g", "w"}));
        List<String> array = new ArrayList<>(List.of(new String[]{"b", "b", "l", "l", "g", "n", "o", "a", "w"}));
        String word = "balloon";

        List<String> actual = removeLetters.removeLetters(array, word);

        assertEquals(expected, actual);
    }

    @Test
    void removeLetters_listOfStringsAndWord_shouldReturnEmptyArray() {
        RemoveLetters removeLetters = new RemoveLetters();
        List<String> expected = new ArrayList<>(List.of(new String[]{}));
        List<String> array = new ArrayList<String>(List.of(new String[]{"a", "n", "r", "y", "o", "w"}));
        String word = "norway";

        List<String> actual = removeLetters.removeLetters(array, word);

        assertEquals(expected, actual);
    }

    @Test
    void removeLetters_listOfStringsAndWord_shouldReturntu() {
        RemoveLetters removeLetters = new RemoveLetters();
        List<String> expected = new ArrayList<>(List.of(new String[]{"t", "u"}));
        List<String> array = new ArrayList<String>(List.of(new String[]{"t", "t", "e", "s", "t", "u"}));
        String word = "testing";

        List<String> actual = removeLetters.removeLetters(array, word);

        assertEquals(expected, actual);
    }

}