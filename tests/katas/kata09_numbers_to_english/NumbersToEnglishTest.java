package katas.kata09_numbers_to_english;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumbersToEnglishTest {

    @Test
    void numToEng_theNumber0_ShouldReturnZero() {
        // Arrange
        NumbersToEnglish numbersToEnglish = new NumbersToEnglish();
        String expected = "zero";
        // Act
        String actual = numbersToEnglish.numToEng(0);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void numToEng_theNumber26_ShouldReturnTwentySix() {
        // Arrange
        NumbersToEnglish numbersToEnglish = new NumbersToEnglish();
        String expected = "twenty six";
        // Act
        String actual = numbersToEnglish.numToEng(26);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void numToEng_theNumber519_ShouldReturnFiveHundredNineteen() {
        // Arrange
        NumbersToEnglish numbersToEnglish = new NumbersToEnglish();
        String expected = "five hundred nineteen";
        // Act
        String actual = numbersToEnglish.numToEng(519);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void numToEng_theNumber106_ShouldReturnOneHundredSix() {
        // Arrange
        NumbersToEnglish numbersToEnglish = new NumbersToEnglish();
        String expected = "one hundred six";
        // Act
        String actual = numbersToEnglish.numToEng(106);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void numToEng_theNumber999_ShouldReturnNineHundredNinetyNine() {
        // Arrange
        NumbersToEnglish numbersToEnglish = new NumbersToEnglish();
        String expected = "nine hundred ninety nine";
        // Act
        String actual = numbersToEnglish.numToEng(999);
        // Assert
        assertEquals(expected, actual);
    }

}