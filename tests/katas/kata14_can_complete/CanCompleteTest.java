package katas.kata14_can_complete;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CanCompleteTest {

    @Test
    void canComplete_butl_shouldReturnTrue() {
        // Arrange
        CanComplete canComplete = new CanComplete();
        String incompleteWord = "butl";
        boolean expected = true;
        // Act
        boolean actual = canComplete.canComplete(incompleteWord, "beautiful");
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void canComplete_butlz_shouldReturnFalse() {
        // Arrange
        CanComplete canComplete = new CanComplete();
        String incompleteWord = "butlz";
        boolean expected = false;
        // Act
        boolean actual = canComplete.canComplete(incompleteWord, "beautiful");
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void canComplete_tulb_shouldReturnFalse() {
        // Arrange
        CanComplete canComplete = new CanComplete();
        String incompleteWord = "tulb";
        boolean expected = false;
        // Act
        boolean actual = canComplete.canComplete(incompleteWord, "beautiful");
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void canComplete_bbutl_shouldReturnFalse() {
        // Arrange
        CanComplete canComplete = new CanComplete();
        String incompleteWord = "bbutl";
        boolean expected = false;
        // Act
        boolean actual = canComplete.canComplete(incompleteWord, "beautiful");
        // Assert
        assertEquals(expected, actual);
    }

}