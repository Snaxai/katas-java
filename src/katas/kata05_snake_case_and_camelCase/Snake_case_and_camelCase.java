package katas.kata05_snake_case_and_camelCase;

public class Snake_case_and_camelCase {
    public String toCamelCase(String word) {
        String[] words = word.split("_"); // removes the char
        String camelCaseWord = words[0];
        for (int i = 1; i < words.length; i++) {
            camelCaseWord += words[i].replace(words[i].charAt(0), words[i].toUpperCase().charAt(0));
        }
        return camelCaseWord;
    }

    public String toSnakeCase(String word) {
        // String[] words = word.split("(?=\\p{Upper})");
        String[] words = word.split("(?=[A-Z])");  // does not remove char
        String camelCaseWord = words[0];
        for (int i = 1; i < words.length; i++) {
            camelCaseWord += "_" + words[i].replace(words[i].charAt(0), words[i].toLowerCase().charAt(0));
        }
        return camelCaseWord;
    }
}
