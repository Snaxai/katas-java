package katas.kata02_pascalcase_converter;

public class PascalCaseConverter {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args) {

        pascalCaseConverter("The quick brown fox jumped over the lazy dog");
        // pascalCaseConverter("The quIck brOwn fox juMPed oVEr tHe laZY doG");
        // pascalCaseConverter("The quIck brOwn fox? juMPed. oVEr tHe, laZY! doG.");
        pascalCaseConverter("The qUick!  bRoWn fox    jumped, OVER the    lazy. dog");
    }

    public static void pascalCaseConverter(String sentence) {
        // Convert the sentence to lower case
        String lowerCaseSentence = sentence.toLowerCase();
        // Split the word
        String[] array = lowerCaseSentence.split("\\s+");
        String pascalCaseWord = "";
        for (String word : array) {
            // Removes punctuation
            String wordWithOutPunctuation = word.replaceAll("[^a-zA-Z]", "");
            // Set the first char to upper case
            char firstCharToUpperCase = wordWithOutPunctuation.toUpperCase().charAt(0);
            // Replaces the first char with upper case
            String wordThatStartWithUpperCase = wordWithOutPunctuation.replaceFirst(".", String.valueOf(firstCharToUpperCase));
            // Adds the word to the pascal word string
            pascalCaseWord += wordThatStartWithUpperCase;
        }
        // Print out message(s)
        System.out.println();
        System.out.println(ANSI_BLUE + "Original sentence: " + sentence);
        System.out.println(ANSI_GREEN + "PascalCase word: " + pascalCaseWord);
    }
}
