package katas.kata13_valid_name;

import java.util.Arrays;

public class ValidateName {

    public boolean validName(String name) {
        String[] splittedName = name.split(" ");
        System.out.println(Arrays.toString(splittedName));
        if (splittedName.length <= 1) return false;
        if (splittedName.length > 3) return false;

        if (splittedName.length > 2 && splittedName[0].length() <= 2 && splittedName[1].length() > 2) return false;
        if (splittedName[splittedName.length-1].contains(".")) return false;

            for (int i = 0; i < splittedName.length; i++) {
                char firstChar = splittedName[i].charAt(0);
                if (!Character.isUpperCase(firstChar)) return false;
                if (splittedName[i].contains(".") && splittedName[i].length() > 2) return false;
                if (splittedName[i].length() <= 1) return false;
            }

        return true;
    }
}
