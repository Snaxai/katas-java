package katas.kata06_you_shall_not_pass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YouShallNotPass {
    public String passwordCheck(String password) {
        int criterias = 0;
        StringBuilder builder = new StringBuilder();

        if (password.length() < 6 || password.contains(" ")) {
            System.out.println("\nInvalid password. Password has to be more than 6 chars long and not contain any whitespace");
            return "Invalid";
        }
        if (password.matches("(.*)[A-Z](.*)")) criterias++;
        else builder.append("\nNo uppercase chars");
        if (password.matches("(.*)[a-z](.*)")) criterias++;
        else builder.append("\nNo lowercase chars");
        if (password.matches("(.*)[1-9](.*)")) criterias++;
        else builder.append("\nNo digits");
        if (password.matches("(.*)[^a-zA-Z0-9](.*)")) criterias++;
        else builder.append("\nNo special chars");
        if (password.length() > 8) criterias++;

        System.out.println(builder);

        if (criterias < 3) return "Weak";
        if (criterias >= 3 && criterias < 5) return "Moderate";
        if (criterias >= 5) return "Strong";
        return "No match";
    }
}
