package katas.kata12_validate_card;

public class ValidateCard {
    public boolean validateCard(long number) {
        String numberString = String.valueOf(number);
        if (numberString.length() > 19 || numberString.length() < 14) return false;

        int checkDigit = (int) (number % 10);
        long num = number / 10;

        long reversed = 0;
        while (num != 0) {
            int remainder = (int) (num % 10);
            reversed = reversed * 10 + remainder;
            num = num / 10;
        }

        String numbers = String.valueOf(reversed);
        int[] digits = new int[numbers.length()];
        for (int i = 0; i < numbers.length(); i++) {
            int digit = numbers.charAt(i) - '0';
            if (i % 2 != 0 || i == 1) { // Noe feil her
                digit += digit;
                if (digit > 9) {
                    digit = 1 + (digit % 10);
                }
            }
            digits[i] = digit;
        }

        int allDigits = 0;
        for (int j = 0; j < digits.length; j++) {
            allDigits += digits[j];
        }

        if (checkDigit == (10 - (allDigits % 10))) return true;
        else return false;
    }
}
