package katas.kata03_largest_difference_in_array;

import java.lang.reflect.Array;

public class LargestDifference {
    public static void main(String[] args) {
        addTwoNumbers(new int[]{2, 3, 1, 7, 9, 5, 11, 3, 5}, 10);

        // Expect 10
        findLargestDifference(new int[]{2, 3, 1, 7, 9, 5, 11, 3, 5});

        // Expect 12
        findLargestDifference(new int[]{1, 3, 1, 7, 9, 13, 11, 3, 5});

        // Expect 21
        findLargestDifference(new int[]{2, 3, 4, 7, 9, 5, 23, 1, 3});

        // Expect 8
        findLargestDifference(new int[]{20, 3, 7, 7, 9, 5, 11, 2, 5});
    }

    public static void findLargestDifference(int[] array) {
        int low = array[0];
        int difference = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] - low > difference) difference = array[i] - low;
            if (array[i] < low) {
                low = array[i];
            }
        }
        System.out.println(difference);
    }

    public static void addTwoNumbers(int[] array, int number) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] + array[j] == number) {
                    System.out.println(array[i] + " + " + array[j] + " = " + number);
                    System.out.println();
                }
            }
        }
    }
}
