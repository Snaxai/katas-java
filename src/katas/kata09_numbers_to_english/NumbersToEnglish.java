package katas.kata09_numbers_to_english;

public class NumbersToEnglish {
    private static final String[] tensNames = {
            "",
            " ten",
            " twenty",
            " thirty",
            " forty",
            " fifty",
            " sixty",
            " seventy",
            " eighty",
            " ninety"
    };

    private static final String[] numNames = {
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen"
    };

    public String numToEng(int number) {
        String numbersToWords;
        if (number == 0) return "zero";

        if (number % 100 < 20) {
            numbersToWords = numNames[number % 100];
            number /= 100;
        } else {
            numbersToWords = numNames[number % 10];
            number /= 10;

            numbersToWords = tensNames[number % 10] + numbersToWords;
            number /= 10;
        }
        if (number == 0) return numbersToWords.trim();
        return (numNames[number] + " hundred" + numbersToWords).trim();
    }
}
