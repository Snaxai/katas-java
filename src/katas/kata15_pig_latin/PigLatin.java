package katas.kata15_pig_latin;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PigLatin {
    public String translateWordToPigLatin(String word) {
        boolean firstCharUppercase = false;
        int indexOfVowel = findVowelIndex(word);
        if (indexOfVowel == -1) return "";
        if (Character.isUpperCase(word.charAt(0))) {
            firstCharUppercase = true;
        }

        String wordInLowerCase = word.toLowerCase(Locale.ROOT);
        List<String> punctuation = List.of(word.split("(?=\\p{Punct})"));
        System.out.println(punctuation);
        wordInLowerCase = wordInLowerCase.replaceAll("\\p{Punct}", "");

        if (indexOfVowel == 0) {
            if (firstCharUppercase) {
                char upperCaseChar = wordInLowerCase.toUpperCase().charAt(0);
                wordInLowerCase = wordInLowerCase.replaceFirst(".", String.valueOf(upperCaseChar));
            }
            return wordInLowerCase + "yay";
        } else {
            String firstPart = wordInLowerCase.substring(indexOfVowel);
            String lastPart = wordInLowerCase.substring(0, indexOfVowel);
            String pigLatinWord = firstPart + lastPart + "ay";
            if (firstCharUppercase) {
                char upperCaseChar = pigLatinWord.toUpperCase().charAt(0);
                pigLatinWord = pigLatinWord.replaceFirst(".", String.valueOf(upperCaseChar));
            }
            if (punctuation.size() > 1) {
                pigLatinWord = pigLatinWord + punctuation.get(1);
            }
            return pigLatinWord;
        }
    }

    public String translateSentenceToPigLatin(String sentence) {
        String[] wordsInSentence = sentence.split(" ");
        String pigLatinSentence = "";
        for (String word : wordsInSentence) {
            pigLatinSentence += " " + translateWordToPigLatin(word);
        }
        System.out.println(pigLatinSentence);
        return pigLatinSentence.trim();
    }

    public int findVowelIndex(String input) {
        Pattern pattern = Pattern.compile("[aeiouAEIOU]");
        List<String> listOfChars = List.of(input.split(""));
        for (int i = 0; i < listOfChars.size(); i++) {
            Matcher matcher = pattern.matcher(listOfChars.get(i));
            if (matcher.find()) return i;
        }
        return -1;
    }

}
