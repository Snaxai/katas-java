package katas.kata10_bookstore;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Bookstore {

    public double price(int[] books) {
        double discount = 1;
        double priceOfBook = 8;
        int numberOfBooks = books.length;

        if (books.length == 0) return 0;
        if (books.length == 1) return 8;

        Set<Integer> bookSet = IntStream.of(books).boxed().collect(Collectors.toSet());

        System.out.println("Books: " + Arrays.toString(books));
        System.out.println("Bookset:" + bookSet);

        int duplicateBooks = books.length - bookSet.size();

        if (bookSet.size() == 2) discount = 0.95;
        if (bookSet.size() == 3) discount = 0.90;
        if (bookSet.size() >= 4) discount = 0.80;

        return (priceOfBook * numberOfBooks * discount);
    }
}
