package katas.kata11_remove_letters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RemoveLetters {

    public List<String> removeLetters(List<String> words, String word) {
        List<String> wordArr = new ArrayList<>(List.of(word.split("(?!^)")));
        List<String> result = new ArrayList<>(words);

        Iterator<String> iterator = wordArr.iterator();

        while (iterator.hasNext()) {
            String name = iterator.next();
            if (wordArr.contains(name)) {
                iterator.remove();
                result.remove(name);
            }
        }
        return result;
    }
}
