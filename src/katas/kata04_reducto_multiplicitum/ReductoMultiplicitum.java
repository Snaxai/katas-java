package katas.kata04_reducto_multiplicitum;

public class ReductoMultiplicitum {
    public static void main(String[] args) {

    }

    public int sumDigProd(int... numbers) {
        int sum = 0;
        if (numbers.length > 1) {
            for (int i = 0; i < numbers.length; i++) {
                sum += numbers[i];
            }
        } else sum = numbers[0];

        String[] digits = String.valueOf(sum).split("");
        if (digits.length <= 1) return Integer.parseInt(digits[0]);
        int product = 1;
        for (int i = 0; i < digits.length; i++) {
            product *= Integer.valueOf(digits[i]);
        }
        if (product > 9) {
            return sumDigProd(product);
        } else return product;
    }
}
