package katas.kata14_can_complete;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CanComplete {
    public boolean canComplete(String incompleteWord, String wordToBeMatched) {
        int lastIndex = 0;
        for (Character currentChar : incompleteWord.toCharArray()) {
            int currentIndex = wordToBeMatched.indexOf(currentChar.toString(), lastIndex);
            if (currentIndex < lastIndex) return false;
            lastIndex = currentIndex + 1;
        }
        return true;
    }
}
