package katas.kata08_human_readable_time;

public class HumanReadableTime {
    public String convertToReadableTime(int seconds) {
        if (seconds > 359999) {
            return "Error";
        }
        int findHours = seconds / 3600;
        int findMinutes = (seconds % 3600) / 60;
        int findSeconds = (seconds % 60);

        return String.format("%02d:%02d:%02d", findHours, findMinutes, findSeconds);
    }
}
