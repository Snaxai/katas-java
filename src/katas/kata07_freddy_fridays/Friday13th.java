package katas.kata07_freddy_fridays;


import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Friday13th {
    public static void main(String[] args) {
    }

    public int countFriday13th(int year) {
        DateTimeFormatter myFormat = DateTimeFormatter.ofPattern("E");
        int fridays = 0;
        for (int i = 1; i < 12; i++) {
            String formattedDate = LocalDateTime.of(year, i, 13, 1, 1).format(myFormat);
            if (formattedDate.contains("Fri")) {
                fridays++;
            }
            System.out.println(formattedDate);
        }
        System.out.println("Number of fridays: " + fridays);
        return fridays;
    }
}
